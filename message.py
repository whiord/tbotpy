﻿#-------------------------------------------------------------------------------
# Name:        модуль1
# Purpose:
#
# Author:      whiord
#
# Created:     29.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class Message:
    TERMINATE = -100
    WAIT  = -10
    ERROR = -1
    LOGIN = 0
    GET_VILLAGES = 100
    SEND_TROOP = 200
    SEND_TROOP_INS = 201
    def __init__(self, type, *data, **data2):
        self.type = type
        self.data = data
        self.data2 = data2