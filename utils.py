﻿#-------------------------------------------------------------------------------
# Name:        РјРѕРґСѓР»СЊ1
# Purpose:
#
# Author:      whiord
#
# Created:     27.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import re
import errors

class Utils:
    @staticmethod
    def parseParams(arg):
        pass

    @staticmethod
    def parsePairs(str_):
        str_ += " "
        res = {}
        state = "No"
        left = ""
        right =""
        #print str_
        for i in str_:
            if state == "No":
                if i != " " and i != "\t" and i != "\n" and i != "\r":
                    left = i
                    state = "Left"
                    #print "No", left, "|", right
            elif state == "Left":
                if i == " " or i == "\t" or i == "\n" or i == "\r":
                    state = "No"
                    res[left] = ""
                    #print "L eft", left, "|", right
                elif i == "=":
                    state = "Consume"
                    #print "L=eft", left, "|", right
                else:
                    left += i
                    #print "Left", left, "|", right
            elif state == "Consume":
                state = "Right"
                right = ""
                #print "Consume quote"
            elif state == "Right":
                if i == "\"":
                    state = "No"
                    res[left] = right
                    left = ""
                    right = ""
                    #print "Right\"", left, "|" + i, right
                else:
                    right += i
                    #print "Right", left, "||" + i, right
        return res

    @staticmethod
    def parseForm(text, name = None):
        if name is None:
            form = re.search("<form[^>]*>(.*?)(?=</form>)", text, re.S)
        else:
            form = re.search("<form[^>]*name=\"{0}\"[^>]*>(.*?)(?=</form>)".format(name), text, re.S)
        if form is None:
            raise errors.MissingFormError()
        else:
            inputs = re.findall("<input([^>]*)/>", form.group(0))
            res = {}
            for i in inputs:
                pairs = Utils.parsePairs(i)
                #print pairs
                if not pairs.has_key("value"): pairs["value"] = ""
                res[pairs["name"]] = pairs["value"]
            return res

    @staticmethod
    def parseArmySend(text):
        army = re.findall(r"document\.snd\.t(.+)\.value=(\d+)", text)
        res = {}
        for i in army:
            res[int(i[0])-1] = i[1]
        return res

    @staticmethod
    def parseVillages(text):
        vil = re.findall(r"<a href=\"(\?newdid[^\"]*)\"[^>]*class=\"([^\"]*)\"[^>]*>(.*?)(?=</a>)", text, re.S)
        res=[]
        for i in vil:
            buf = {}
            buf["href"] = i[0]
            buf["class"] = i[1]
            buf["name"] = i[2]
            res.append(buf)
        return res
