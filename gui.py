﻿#-------------------------------------------------------------------------------
# Name:        gui
# Purpose:
#
# Author:      whiord
#
# Created:     25.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class GUI:
    @staticmethod
    def start():
        raise NotImplementedError()

if __name__ == "__main__":
    GUI.start()