﻿#-------------------------------------------------------------------------------
# Name:        main
# Purpose:
#
# Author:      whiord
#
# Created:     25.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    import sys
    if len(sys.argv) > 1 and sys.argv[1] == "--gui":
        from gui import GUI
        GUI.start()
    else:
        from console import Console
        Console.start()

if __name__ == '__main__':
    main()