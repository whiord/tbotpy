﻿#-------------------------------------------------------------------------------
# Name:        gameController
# Purpose:
#
# Author:      whiord
#
# Created:     25.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import requests
import errors
import time
import random
from message import Message
from utils import Utils
from multiprocessing import Process, Queue


def createGC(evq_in, evq_out):
    GameController.inst = GameController(evq_in, evq_out)
    GameController.inst.activity()

class GameController:
    def __init__(self, in_, out_):
        #Process.__init__(self, target = self.activity, args=(self,))

        #print "!!!"
        self.evq_in = in_
        self.evq_out = out_

        self.logged_in = False
        self.loginPage = "login.php"
        self.mainPage = "dorf1.php"
        self.buildPage = "build.php"
        self.session = requests.Session()

        self.session.headers["User-Agent"] = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0"
        self.session.headers["Host"] = "ts6.travian.ru"
        self.session.headers["Connection"] = "keep-alive"
        self.session.headers["Accept-Language"] = "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"
        self.session.headers["Accept-Encoding"] = "gzip, deflate"
        self.session.headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        #print "!!!!"

    @staticmethod
    def startGC(evq_in, evq_out):
        p = Process(target = createGC, args = (evq_in, evq_out))
        p.start()
        return p

    def activity(self):
        #print "???"
        while True:
            msg = self.evq_in.get()
            try:
                if msg.type == Message.LOGIN:
                    self.login(*msg.data, **msg.data2)
                    time.sleep(2)
                    self.evq_out.put(Message(Message.LOGIN, "Logged in successfully."))
                elif msg.type == Message.GET_VILLAGES:
                    vil = self.getVillages()
                    time.sleep(random.random())
                    self.evq_out.put(Message(Message.GET_VILLAGES, vil))
                elif msg.type == Message.SEND_TROOP:
                    try:
                        self.sendTroop(**msg.data[0])
                        time.sleep(random.random())
                        self.evq_out.put(Message(Message.SEND_TROOP, "Troop was sent successfully."))
                    except errors.InsufficientArmyError:
                        self.evq_out.put(Message(Message.SEND_TROOP_INS, "Insufficient army/"))
                elif msg.type == Message.WAIT:
                    time.sleep(float(msg.data[0]))
                elif msg.type == Message.TERMINATE:
                    self.evq_out.put(Message(Message.TERMINATE, "Process goint to terminate..."))
                    break
            except errors.ConnectionError:
                self.evq_out.put(Message(Message.ERROR, "Connection error/"))

    def sendTroop(self, **argv):
        url = self.baseURL + self.buildPage
        pars = {"id":39, "tt":2}
        page1 = self.session.get(url, params = pars)
        r1 = Utils.parseForm(page1.text, "snd")
        army = Utils.parseArmySend(page1.text)
        for i in enumerate(argv["army"]):
            if int(i[1])!=0:
                if not army.has_key(i[0]) or int(army[i[0]]) < int(i[1]):
                    raise errors.InsufficientArmyError()
                else:
                    r1["t"+str(i[0]+1)] = int(i[1])

        r1["c"] = 4
        r1["x"] = argv["to"][0]
        r1["y"] = argv["to"][1]
        page2 = self.session.post(url, params = pars, data = r1)
        r2 = Utils.parseForm(page2.text)
        time.sleep(0.5 + random.random())
        page3 = self.session.post(url, params = pars, data = r2)
        pass

    def testConnection(self, request):
        if request.headers["set-cookie"]:
            self.session.cookies = request.cookies
            return True
        else:
            try:
                Utils.parseForm(request.text, "login")
            except errors.MissingFormError:
                return True
            if self.reconnect:
                self.logged_in = False
                self.login(self.country, self.server,self.logn, self.password, True)
            else:
                raise errors.ConnectionError()


    def getVillages(self):
        if self.logged_in:
            page = self.session.get(self.baseURL + self.mainPage)
            self.testConnection(page)
            self.vil = Utils.parseVillages(page.text)
            return self.vil

    def login(self, country, server, logn, password, reconnect = False):
        if self.logged_in:
            page = self.session.get(self.baseURL + self.mainPage)
            return self.testConnection(page)
        else:
            self.country = country
            self.server = server
            self.logn = logn
            self.password = password
            self.reconnect = reconnect

            self.baseURL = "http://" + server + ".travian." + "ru/"
            loginForm = self.session.get(self.baseURL + self.loginPage)
            parsed = Utils.parseForm(loginForm.text, "login")
            if not parsed.has_key("login"):
                raise errors.LoginFormError()
            else:
                parsed["name"] = logn
                parsed["password"] = password
                parsed["w"] = "1280:800"
                r = requests.post(self.baseURL + self.mainPage, parsed)
                #print(r.headers)
                if not r.headers.has_key("set-cookie"):
                    raise errors.LoginError()
                else:
                    self.session.cookies = r.cookies
                    self.logged_in = True
                    return True
