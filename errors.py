﻿#-------------------------------------------------------------------------------
# Name:        модуль1
# Purpose:
#
# Author:      whiord
#
# Created:     29.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

class MissingFormError(Exception):
    pass

class LoginFormError(Exception):
    pass

class LoginError(Exception):
    pass

class ConnectionError(Exception):
    pass

class InsufficientArmyError(Exception):
    pass