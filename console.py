﻿#-------------------------------------------------------------------------------
# Name:        Console
# Purpose:
#
# Author:      whiord
#
# Created:     25.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import time

class Console:
    @staticmethod
    def initCommandLoop():
        from command import Command
        try:
            while True:
                try:
                    cmd = raw_input(">").split()
                except EOFError:
                    Command.quit()
                    break
                if len(cmd) > 0:
                    if cmd[0] == "quit":
                        Command.quit()
                        break
                    else:
                        #try:
                            print(cmd)
                            Command.execute(cmd)
                            #eval("Command.{0}({1})".format(cmd[0], cmd[1:]))
                        #except Exception:
                        #print("Something wrong you are doing!")
        finally:
            Command.quit()

    @staticmethod
    def start():
        print("Welcome to TBot!")
        Console.initCommandLoop()

if __name__ == "__main__":
    Console.start()