﻿#-------------------------------------------------------------------------------
# Name:        command
# Purpose:
#
# Author:      whiord
#
# Created:     25.01.2013
# Copyright:   (c) whiord 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

#gameController = None
from gameController import GameController
from multiprocessing import Queue
from message import Message
import time

evq_in = Queue()
evq_out = Queue()
print("Starting GC...")
gcp = GameController.startGC(evq_out, evq_in)
print("GameController started in thread.")

import errors

class Default:
    login = ["ru", "ts6", "darord", "qwertyloop"]

class Command:
    @staticmethod
    def quit():
        evq_out.put(Message(Message.TERMINATE))

    @staticmethod
    def hello(argv):
        print("Hello, world!")

    @staticmethod
    def execute(cmd):
        eval("Command.{0}({1})".format(cmd[0], cmd[1:]))

    @staticmethod
    def script(argv):
        f = open(argv[0], "r")
        for i in f.readlines():
            #print i
            Command.execute(i.split())
        f.close()

    @staticmethod
    def wait(argv):
        if argv[0] == "-con":
            time.sleep(argv[1])
        else:
            evq_out.put(Message(Message.WAIT, argv[0]))

    @staticmethod
    def get(argv):
        if len(argv) == 0: num = 1
        else: num = int(argv[0])
        try:
            for i in xrange(num):
                msg = evq_in.get_nowait()
                print msg.type, msg.data
        except:
            print "Empty queue."

    @staticmethod
    def login(argv):
        if len(argv) == 0:
            print("Usage: login country server login password")
            print("Example: login ru ts6 www 1234")
        else:
            if "-d" in argv: d = True
            else: d = False
            if "-r" in argv: r = True
            else: r = False
            if d:
                evq_out.put(Message(Message.LOGIN, *Default.login, reconnect = r))
            else:
                evq_out.put(Message(Message.LOGIN, *argv, reconnect = r))


    @staticmethod
    def villages(argv):
        evq_out.put(Message(Message.GET_VILLAGES))
        #vil = gc.getVillages()

    @staticmethod
    def send(argv):
        if argv[0] == "troop":
            msg = Message(Message.SEND_TROOP, {"from": argv[1], "to": (argv[2], argv[3]), "army": argv[4:]})
            evq_out.put(msg)

    @staticmethod
    def logout(argv):
        pass